<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lin_file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path',500)->comment('地址');
            $table->string('type',10)->comment('LOCAL 本地，REMOTE 远程');
            $table->string('name',100)->comment('文件名');
            $table->string('extension',50)->nullable()->comment('扩展名');
            $table->integer('size')->nullable()->comment('字节');
            $table->string('md5',40)->nullable()->comment('md5值，防止上传重复文件');
            $table->dateTime('create_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('update_time')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->dateTime('delete_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lin_file');
    }
}
