<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinGroupPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lin_group_permission')->insert([
            [
                'group_id' => 3,
                'permission_id' => 1,
            ], [
                'group_id' => 3,
                'permission_id' => 2,
            ], [
                'group_id' => 3,
                'permission_id' => 4,
            ], [
                'group_id' => 4,
                'permission_id' => 3,
            ]
        ]);
    }
}
