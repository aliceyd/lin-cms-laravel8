<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lin_user')->insert([
            'username' => 'super',
            'nickname' => 'super',
            'email' => '123456@qq.com',
        ]);
    }
}
