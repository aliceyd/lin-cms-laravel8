<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LinGroupPermissionTableSeeder::Class);
        $this->call(LinGroupTableSeeder::Class);
        $this->call(LinPermissionTableSeeder::Class);
        $this->call(LinUserGroupTableSeeder::Class);
        $this->call(LinUserIdentityTableSeeder::Class);
        $this->call(LinUserTableSeeder::Class);
    }
}
