<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lin_group')->insert([
            [
                'name' => 'root',
                'info' => '超级用户组',
                'level' => 1,
            ], [
                'name' => 'guest',
                'info' => '游客组',
                'level' => 3,
            ], [
                'name' => '前端工程师',
                'info' => '只有管理日志的权限',
                'level' => 2,
            ], [
                'name' => '后端工程师',
                'info' => '只有删除图书的权限',
                'level' => 2,
            ]
        ]);
    }
}
