<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 创建超级管理员
        DB::table('lin_user')->insert([
            'username' => 'super',
            'nickname' => 'super',
            'email' => '123456@qq.com',
        ]);
        $user_id = DB::table('lin_user')->where('username', 'super')->value('id');
        DB::table('lin_user_identity')->insert([
            'user_id' => $user_id,
            'identifier' => 'super',
            'credential' => md5('123456'),
        ]);
        DB::table('lin_user_group')->insert([
            'user_id' => $user_id,
            'group_id' => 1,
        ]);
    }
}
