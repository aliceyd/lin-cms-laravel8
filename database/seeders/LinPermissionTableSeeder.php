<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lin_permission')->insert([
            [
                'name' => '查询所有日志',
                'module' => '日志',
                'mount' => 1,
            ], [
                'name' => '搜索日志',
                'module' => '日志',
                'mount' => 1,
            ], [
                'name' => '删除图书',
                'module' => '图书',
                'mount' => 1,
            ], [
                'name' => '查询日志记录的用户',
                'module' => '日志',
                'mount' => 1,
            ]
        ]);
    }
}
