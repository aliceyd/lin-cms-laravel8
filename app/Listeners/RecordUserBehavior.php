<?php

namespace App\Listeners;

use App\Events\Logger;
use App\Exceptions\lin\LinException;
use App\Models\lin\LinLog;
use App\Services\token\LoginToken;
use Illuminate\Contracts\Queue\ShouldQueue; #implements ShouldQueue
use Illuminate\Queue\InteractsWithQueue; #use InteractsWithQueue;

class RecordUserBehavior
{
    /* 将事件加入队列
     * Class implements ShouldQueue
     * use InteractsWithQueue;
     * handle() 设置延迟执行时间 if (true) { $this->release(10); }
     */

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Logger $event
     * @return void
     * @throws LinException
     */
    public function handle(Logger $event)
    {
        //
        $params = $event->params;
        if (empty($params)) {
            throw new LinException([
                'msg' => '日志信息不能为空'
            ]);
        }
        if (is_array($params)) {
            list('uid' => $uid, 'username' => $username, 'msg' => $message) = $params;
        } else {
            $tokenService = LoginToken::getInstance();
            $uid = $tokenService->getCurrentUid();
            $username = $tokenService->getCurrentUserName();
            $message = $params;
        }
        $data = [
            'message' => $username . $message,
            'user_id' => $uid,
            'username' => $username,
            'status_code' => 200,
            'method' => \Request::method(),
            'path' => \Request::path(),
            'permission' => '', #权限
        ];
        LinLog::create($data);
    }
}
