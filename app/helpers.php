<?php

// 应用公共文件

function writeJson($code = 0, $status = 201, $msg = 'ok', $data = [])
{
    $re = (new \App\Lib\CodeMessage())->codeMessage($code);
    $data = [
        'code' => $re['code'] ?? $code,
        'message' => $re['msg'] ?? $msg,
        'request' => \Request::method() . ' ' . \Request::path(),
    ];
    return response()->json($data, $status);
}

function paginate()
{
    $count = intval(Request::get('count')) ?: 15;
    $page = intval(Request::get('page'));
    $count = $count >= 15 ? 15 : $count;

    $start = $page * $count;

    if ($start < 0 || $count < 0) throw new \App\Exceptions\BaseException();

    return [$start, $count, $page];
}

function listen($log)
{
    event(new \App\Events\Logger($log));
}
