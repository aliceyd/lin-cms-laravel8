<?php

namespace App\Http\Controllers\v1;

use App\Exceptions\cms\CmsException;
use App\Http\Requests\cms\bookRequest;
use App\Models\cms\Book as BookModel;

class Book
{
    /**
     * 查询指定bid的图书
     */
    public function getBook($bid)
    {
        $book = BookModel::find($bid);
        if(!$book) throw new CmsException(10022);
        return $book;
    }

    /**
     * 查询所有图书
     */
    public function getBooks()
    {
        return BookModel::all();
    }

    /**
     * 新建图书
     */
    public function create(bookRequest $request)
    {
        $params = $request->post();
        BookModel::create($params);
        return writeJson(12);
    }

    /**
     * 更新图书
     */
    public function update(bookRequest $request, $bid)
    {
        $params = $request->post();
        $book = $this->getBook($bid);
        $book->update($params);
        return writeJson(13);
    }

    /**
     * 删除图书
     */
    public function delete($bid)
    {
        $book = $this->getBook($bid);
        $book->delete();
        return writeJson(14);
    }
}
