<?php

namespace App\Http\Controllers\cms;

use Illuminate\Routing\Controller;

class Api extends Controller
{
    public function getApis()
    {
        $json_string = file_get_contents(public_path('lin-cms.json'));
        $json_string = str_replace(array("\n", "\t"), "", $json_string);
        $data = json_decode($json_string, true);
        $api = $params = [];
        foreach ($data['item'] as $value) {
            foreach ($value['item'] as $item) {
                if (isset($item['request']['url']['query'])) {
                    foreach ($item['request']['url']['query'] as $param){
                        $params[] = [
                            'name' => $param['key'],
                            'required' => true,
                            'default' => '无' ?? $param['value'],
                            'description' => '含义',
                        ];
                    }
                }
                $api[] = [
                    'name' => $item['name'],
                    'url' => '/' . implode('/', $item['request']['url']['path']),
                    'method' => $item['request']['method'],
                    'param' => $params ?? '',
                    'body' => $item['request']['body']['raw'] ?? '',
                    'result' => $item['response'][0]['body'] ?? '',
                ];
            }
        }
        return $api;
    }
}
