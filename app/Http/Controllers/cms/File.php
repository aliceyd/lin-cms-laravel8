<?php

namespace App\Http\Controllers\cms;

use App\Exceptions\lin\LinException;
use App\Lib\file\LocalUploader;
use Illuminate\Http\Request;

class File
{
    public function uploadImage(Request $request)
    {
        try {
            $request = $request->file();
        } catch (\Exception $e) {
            throw new LinException(10161);
        }

        return (new LocalUploader($request))->upload();
    }
}
