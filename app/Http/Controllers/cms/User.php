<?php

namespace App\Http\Controllers\cms;

use App\Exceptions\BaseException;
use App\Exceptions\lin\UserException;
use Illuminate\Routing\Controller;

use App\Http\Requests\lin\changePasswordRequest;
use App\Http\Requests\lin\userInfoRequest;
use App\Http\Requests\lin\userRegisterRequest;
use App\Models\lin\LinUser;
use App\Models\lin\LinUserIdentity;
use App\Services\token\LoginToken;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class User extends Controller
{
    private $loginTokenService;

    public function __construct()
    {
        $this->loginTokenService = LoginToken::getInstance();
    }

    /**
     * 登录
     * @param Request $request
     * @return array
     * @throws UserException
     * @throws BaseException
     */
    public function login(Request $request)
    {
        $params = $request->post();
        $user = LinUserIdentity::verify($params['username'], $params['password']);
        $extend['id'] = $user->user_id;
        $extend['username'] = $user->identifier;
        $token = $this->loginTokenService->getToken($extend);
        listen(array('uid' => $user->user_id, 'username' => $user->identifier, 'msg' => '登陆成功获取了令牌'));
        return [
            'access_token' => $token['accessToken'],
            'refresh_token' => $token['refreshToken']
        ];
    }

    /**
     * 用户更新信息
     * @param Request $request
     * @return false|string
     * @throws UserException
     */
    public function update(userInfoRequest $request)
    {
        $params = $request->input();
        $uid = $this->loginTokenService->getCurrentUid();
        LinUser::updateUserInfo($uid, $params);
        return writeJson(2);
    }

    /**
     * 修改密码
     * @param changePasswordRequest $request
     * @return JsonResponse
     * @throws UserException
     */
    public function changePassword(changePasswordRequest $request)
    {
        $uid = $this->loginTokenService->getCurrentUid();
        LinUserIdentity::changePassword($uid, $request->all());

        listen('修改了自己的密码');
        return writeJson(4);
    }

    /**
     * 查询自己拥有的权限
     */
    public function getAllowedApis()
    {
        $uid = $this->loginTokenService->getCurrentUid();
        return LinUser::getUserByUID($uid);
    }

    /**
     * 创建用户
     * @param userRegisterRequest $request
     * @return JsonResponse
     * @throws UserException
     */
    public function register(userRegisterRequest $request)
    {
        LinUser::createUser($request->post());
        listen('创建了一个用户');
        return writeJson(11);
    }

    /**
     * 查询自己的信息
     */
    public function getInformation()
    {
        $uid = $this->loginTokenService->getCurrentUid();
        return LinUser::with('groups')->find($uid);
    }

    /**
     * 刷新令牌
     * @return array
     * @throws BaseException
     */
    public function refresh()
    {
        $token = $this->loginTokenService->getTokenFromHeaders();
        $token = $this->loginTokenService->refresh($token);

        return ['access_token' => $token['accessToken']];
    }
}
