<?php

namespace App\Http\Controllers\cms;

use App\Exceptions\lin\LinException;
use Illuminate\Routing\Controller;
use App\Http\Requests\lin\changePasswordRequest;
use App\Http\Requests\lin\createGroupRequest;
use App\Http\Requests\lin\getUsersRequest;
use App\Http\Requests\lin\permissionRequest;
use App\Http\Requests\lin\updateUserRequest;
use App\Lib\auth\AuthMap;
use App\Models\lin\LinGroup;
use App\Models\lin\LinGroupPermission;
use App\Models\lin\LinPermission;
use App\Models\lin\LinUser;
use App\Models\lin\LinUserGroup;
use App\Models\lin\LinUserIdentity;

class Admin extends Controller
{
    /**
     * 查询所有权限组
     * @return mixed
     */
    public function getGroupAll()
    {
        return LinGroup::where('name', '<>', 'root')->get()->makeVisible('level');
    }

    /**
     * 查询所有可分配的权限
     */
    public function authority()
    {
        return (new AuthMap())->getAuthList();
    }

    /**
     * 查询所有用户
     */
    public function getUsers(getUsersRequest $request)
    {
        $params = $request->all();
        return LinUser::getAdminUsers($params);
    }

    /**
     * 修改用户密码
     */
    public function changeUserPassword(changePasswordRequest $request, $id)
    {
        $params = $request->input();
        $params['uid'] = $id;
        LinUserIdentity::resetPassword($params);
        return writeJson(2);
    }

    /**
     * 删除用户
     */
    public function deleteUser($uid)
    {
        LinUser::deleteUser($uid);
        listen('删除了用户id为' . $uid . '的用户');
        return writeJson(3);
    }

    /**
     * 管理员更新用户信息
     */
    public function updateUser(updateUserRequest $request, $id)
    {
        $params = $request->input();
        $params['uid'] = $id;
        LinUser::updateUser($params);

        return writeJson(6);
    }

    /**
     * 查询一个权限组及其权限
     */
    public function getGroup($id)
    {
        $re = LinGroup::with('permissions')->find($id);
        if (empty($re)) throw new LinException(10024, 404);
        return $re->makeHidden('level');
    }

    /**
     * 新建权限组
     */
    public function createGroup(createGroupRequest $request)
    {
        $params = $request->post();
        LinGroup::createGroup($params);
        return writeJson(15);
    }

    /**
     * 更新一个权限组
     */
    public function updateGroup(createGroupRequest $request, $id)
    {
        $params = $request->all();
        $params['id'] = $id;
        LinGroup::updateGroup($params);
        return writeJson(7);
    }

    /**
     * 删除一个权限组
     */
    public function deleteGroup($id)
    {
        if ($id == 1) throw new LinException(10074, 403);
        if ($id == 2) throw new LinException(10075, 403);
        $hasUser = LinUserGroup::whereGroupId($id)->first();
        if ($hasUser) throw new LinException(10027, 403);
        $group = LinGroup::find($id);
        if ($group) {
            $group->permissions()->detach();
            $group->delete();
        } else {
            throw new LinException(10024, 404);
        }

        listen('删除了权限组id为' . $id . '的权限组');
        return writeJson(8);
    }

    /**
     * 分配单个权限
     */
    public function dispatchPermission(permissionRequest $request)
    {
        $params = $request->post();
        LinGroupPermission::firstOrCreate($params);
        listen('修改了id为' . $params['group_id'] . '的权限');
        return writeJson(9);
    }

    /**
     * 分配多个权限
     */
    public function dispatchPermissions(permissionRequest $request)
    {
        $params = $request->post();
        LinPermission::dispatchAuths($params);
        listen('修改了id为' . $params['group_id'] . '的权限');
        return writeJson(9);
    }

    /**
     * 删除多个权限
     */
    public function removePermissions(permissionRequest $request)
    {
        $params = $request->post();
        LinGroupPermission::whereGroupId($params['group_id'])->whereIn('permission_id', $params['permission_ids'])->delete();
        return writeJson(10);
    }
}
