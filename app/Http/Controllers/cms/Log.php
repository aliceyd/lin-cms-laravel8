<?php


namespace App\Http\Controllers\cms;

use Illuminate\Routing\Controller;
use App\Models\lin\LinLog;
use Illuminate\Http\Request;

class Log extends Controller
{
    /**
     * 查询所有日志
     */
    public function getLogs(Request $request)
    {
        $params = $request->input();
        return LinLog::getLogs($params);
    }

    /**
     * 搜索日志
     */
    public function searchLogs(Request $request)
    {
        $params = $request->input();
        return LinLog::getLogs($params);
    }

    /**
     * 查询日志记录的用户
     */
    public function getUsers(Request $request)
    {
        $params = $request->input();
        return LinLog::getUsers($params);
    }
}
