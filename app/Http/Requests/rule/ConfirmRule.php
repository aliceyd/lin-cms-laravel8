<?php


namespace App\Http\Requests\rule;

use Illuminate\Contracts\Validation\Rule;

class ConfirmRule implements Rule
{
    private $confirm;

    public function __construct($value)
    {
        $this->confirm = $value;
    }

    public function passes($attribute, $value)
    {
        return $value === $this->confirm;
        // TODO: Implement passes() method.
    }

    public function message()
    {
        return 'The :attribute confirmation does not match.';
    }
}
