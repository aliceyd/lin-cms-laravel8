<?php

namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;

class createGroupRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'unique:lin_group,name|required|max:20',
            'info' => 'max:60',
            'permission_ids' => 'array|checkIds'
        ];
    }
}
