<?php


namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;

class permissionRequest extends BaseRequest
{
    public function rules()
    {
        if ($this->post('permission_ids')) {
            return [
                'group_id' => 'required|isPositiveInteger|exists:lin_group,id',
                'permission_ids' => 'required|array|checkIds',
            ];
        }
        return [
            'group_id' => 'required|isPositiveInteger|exists:lin_group,id',
            'permission_id' => 'required|isPositiveInteger|exists:lin_permission,id',
        ];
    }

}
