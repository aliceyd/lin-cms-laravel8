<?php

namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;
use App\Http\Requests\rule\ConfirmRule;

class userRegisterRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'username' => 'unique:lin_user,username|required|max:50',
            'email' => empty($this->email) ? '' : 'unique:lin_user,email|email:rfc,dns',
            'group_ids' => 'array|checkIds',
            'password' => ['required', 'min:2', 'max:6', 'different:old_password', new ConfirmRule($this->confirm_password)],
            'confirm_password' => 'required|min:2|max:6',
        ];
    }
}
