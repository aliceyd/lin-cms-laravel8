<?php


namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;

class userInfoRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'username' => 'unique:lin_user,username|max:50',
            'email' => 'unique:lin_user,email|email:rfc,dns',
            'nickname' => 'max:50',
            'avatar' => 'max:255',
        ];
    }
}
