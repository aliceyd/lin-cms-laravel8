<?php


namespace App\Http\Requests\lin;


use App\Http\Requests\BaseRequest;

class updateUserRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'group_ids' => 'array|checkIds',
            'email' => 'unique:lin_user,email|email:rfc,dns',
        ];
    }
}
