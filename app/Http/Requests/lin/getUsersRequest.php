<?php

namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;

class getUsersRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'group_id' => 'integer|min:2',
            'count' => 'integer',
            'page' => 'integer',
        ];
    }
}
