<?php


namespace App\Http\Requests\lin;

use App\Http\Requests\BaseRequest;
use App\Http\Requests\rule\ConfirmRule;

class changePasswordRequest extends BaseRequest
{
    public function rules()
    {
        $rule = [
            'old_password' => 'required|min:2|max:6',
            'new_password' => ['required', 'min:2', 'max:6', 'different:old_password', new ConfirmRule($this->confirm_password)],
            'confirm_password' => 'required|min:2|max:6',
        ];
        if ($this->route('id')) {
            unset($rule['old_password']);
            return $rule;
        }
        return $rule;
    }
}
