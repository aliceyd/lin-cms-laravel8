<?php

namespace App\Http\Requests\cms;

use App\Http\Requests\BaseRequest;

class bookRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'author' => 'required|max:30',
            'summary' => 'required|max:1000',
            'image' => 'max:100',
        ];
    }
}
