<?php

namespace App\Http\Requests;

use App\Exceptions\lin\LinException;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * 判断用户是否有请求权限
     *
     * @throws LinException
     * @return bool
     */
    public function authorize(): bool
    {
        if (($this->isMethod('put') || $this->isMethod('post'))  && empty($this->all())) {
            throw new LinException(10170);
        }
        return true;
    }

    /*public function attributes()
    {
        return [
            'username' => '用户名',
            'email' => '邮箱',
            'nickname' => '用户昵称',
            'avatar' => '头像',
            'old_password' => '旧密码',
            'new_password' => '新密码',
            'new_password_confirmation' => '确认新密码',
            'group_ids' => '分组Ids',
            'password' => '密码',
            'password_confirmation' => '确认密码',
            'group_id' => 'group_id',
            'count' => '每页数量',
            'page' => '页码',
            'name' => '分组名称',
            'info' => '分组信息',
            'permission_ids' => '权限Ids'
        ];
    }*/

    /*public function messages()
    {
        return [
            '*.check_ids' => 'Ids 必须为正整数',
        ];
    }*/

    /*
    // 验证规则
    public function rules()
    {
        return $this->scene();
    }
    // 验证场景
    protected function scene()
    {
        // 接收当前路由参数
        $id = $this->route('id');
        // 请求方法
        $requestMethod = $this->method();
        // 判断是添加还是更新
        if ($requestMethod == 'POST') {
            // 添加操作
            return [
                'username'  =>  'required',
                'email'     =>  'required|email|unique:users'
            ];
        } else if ($requestMethod == 'PUT') {
            // 删除不需要的错误信息
            unset($this->msg['password.required']);
            unset($this->msg['password.min']);
            // 更新操作
            return [
                'email'     =>  'required|email|unique:users,email,' . $id
            ];
        }
    }*/

}
