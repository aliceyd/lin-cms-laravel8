<?php

namespace App\Models\lin;

use App\Models\BaseModel;

/**
 * App\Models\lin\LinUserGroup
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property int $group_id 分组id
 * @property-read \Illuminate\Database\Eloquent\Collection|LinUserGroup[] $groupPermission
 * @property-read int|null $group_permission_count
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserGroup whereUserId($value)
 * @mixin \Eloquent
 */
class LinUserGroup extends BaseModel
{
    protected $table = 'lin_user_group';

    protected $fillable = ['user_id', 'group_id'];

}
