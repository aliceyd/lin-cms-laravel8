<?php


namespace App\Models\lin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\lin\LinPermission
 *
 * @property int $id
 * @property string $name 权限名称，例如：访问首页
 * @property string $module 权限所属模块，例如：人员管理
 * @property int $mount 0：关闭 1：开启
 * @property string $create_time
 * @property string $update_time
 * @property string|null $delete_time
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereDeleteTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereMount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinPermission whereUpdateTime($value)
 * @mixin \Eloquent
 */
class LinPermission extends BaseModel
{
    use SoftDeletes;

    protected $table = 'lin_permission';

    protected $fillable = ['name', 'module', 'mount'];
    protected $hidden = ['pivot', 'create_time', 'update_time', 'delete_time'];

    public static function dispatchAuths($params)
    {
        foreach ($params['permission_ids'] as $value) {
            $permission = self::find($value);
            if ($permission) {
                $data = [
                    'group_id' => $params['group_id'],
                    'permission_id' => $value,
                ];
                LinGroupPermission::firstOrCreate($data);
            }
        }
    }

    public function getMountAttribute($value)
    {
        $level = [0 => false, 1 => true];
        return $level[$value];
    }
}
