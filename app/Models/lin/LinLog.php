<?php

namespace App\Models\lin;

use App\Models\BaseModel;

/**
 * App\Models\lin\LinLog
 *
 * @property int $id
 * @property string|null $message
 * @property int $user_id
 * @property string|null $username
 * @property int|null $status_code
 * @property string|null $method
 * @property string|null $path
 * @property string|null $permission
 * @property string $create_time
 * @property string $update_time
 * @property string|null $delete_time
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereDeleteTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog wherePermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinLog whereUsername($value)
 * @mixin \Eloquent
 */
class LinLog extends BaseModel
{
    protected $table = 'lin_log';

    protected $fillable = ['message', 'user_id', 'username', 'status_code', 'method', 'path', 'permission'];

    public static function getLogs($params)
    {
        $query = new LinLog();
        if (isset($params['keyword']) && !empty($params['keyword'])) {
            $query = $query->where('message', 'like', '%' . $params['keyword'] . '%');
        }

        if (isset($params['name']) && !empty($params['name'])) {
            $query = $query->whereUsername($params['name']);
        }

        if (isset($params['start']) && !empty($params['start']) && isset($params['end']) && !empty($params['end'])) {
            $query = $query->whereBetween('create_time', [$params['start'], $params['end']]);
        }

        list($start, $count, $page) = paginate();
        $logs = $query->orderBy('create_time','DESC')->get();
        $totalNums = $logs->count();
        return [
            'total' => $totalNums,
            'items' => $logs->skip($start)->take($count),
            'page' => $page,
            'count' => ceil($totalNums / $count) == ($page + 0) ? ($totalNums - $start) : 10,
        ];
    }

    public static function getUsers($params)
    {
        $users = self::distinct()->pluck('username');

        list($start, $count, $page) = paginate();
        return [
            'total' => $users->count(),
            'items' => $users->skip($start)->take($count),
            'page' => $page,
            'count' => $count
        ];
    }
}
