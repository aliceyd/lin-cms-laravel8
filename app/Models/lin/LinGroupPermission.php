<?php

namespace App\Models\lin;

use App\Models\BaseModel;

/**
 * App\Models\lin\LinGroupPermission
 *
 * @property int $id
 * @property int $group_id 分组id
 * @property int $permission_id 权限id
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinGroupPermission wherePermissionId($value)
 * @mixin \Eloquent
 */
class LinGroupPermission extends BaseModel
{
    protected $table = 'lin_group_permission';

    protected $fillable = ['group_id', 'permission_id'];

    public $timestamps = false;

}
