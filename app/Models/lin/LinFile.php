<?php


namespace App\Models\lin;

use App\Models\BaseModel;

/**
 * App\Models\lin\LinFile
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $path 地址
 * @property string $type LOCAL 本地，REMOTE 远程
 * @property string $name 文件名
 * @property string|null $extension 扩展名
 * @property int|null $size 字节
 * @property string|null $md5 md5值，防止上传重复文件
 * @property \Illuminate\Support\Carbon $create_time
 * @property \Illuminate\Support\Carbon $update_time
 * @property string|null $delete_time
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereDeleteTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereMd5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinFile whereUpdateTime($value)
 */
class LinFile extends BaseModel
{
    protected $table = 'lin_file';

    protected $fillable = ['path', 'type', 'name', 'extension', 'size', 'md5'];
}
