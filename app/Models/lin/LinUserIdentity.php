<?php

namespace App\Models\lin;

use App\Exceptions\lin\UserException;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\lin\LinUserIdentity
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property string $identity_type 认证类型，例如 username_password，用户名-密码认证
 * @property string|null $identifier 认证，例如 用户名
 * @property string|null $credential 凭证，例如 密码
 * @property string $create_time
 * @property string $update_time
 * @property string|null $delete_time
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity query()
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereCredential($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereDeleteTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereIdentityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LinUserIdentity whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|LinUserIdentity onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|LinUserIdentity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LinUserIdentity withoutTrashed()
 */
class LinUserIdentity extends BaseModel
{
    protected $table = 'lin_user_identity';

    protected $fillable = ['user_id', 'identifier', 'credential'];

    use SoftDeletes;

    public static function verify($username, $password)
    {
        try {
            $user = self::whereIdentifier($username)->firstOrFail();
        } catch (\Exception $e) {
            throw new UserException();
        }
        if (!self::checkPassword($user->credential, $password)) {
            throw new UserException(10031);
        }
        return $user->makeHidden(['id', 'identity_type', 'credential', 'create_time', 'update_time', 'delete_time']);
    }

    /**
     * @param $uid
     * @param $params
     * @throws UserException
     */
    public static function changePassword($uid, $params)
    {
        $user = self::whereUserId($uid)->first();
        if (!self::checkPassword($user->credential, $params['old_password'])) {
            throw new UserException(10035);
        }
        $user->credential = md5($params['new_password']);
        $user->save();
    }


    public static function resetPassword($params)
    {
        $user = self::whereUserId($params['uid'])->first();
        if (!$user) {
            throw new UserException();
        }
        $user->credential = md5($params['new_password']);
        $user->save();
    }

    private static function checkPassword($md5Password, $password)
    {
        return $md5Password === md5($password);
    }

}
