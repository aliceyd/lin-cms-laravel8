<?php

namespace App\Exceptions\cms;

use App\Exceptions\BaseException;

class CmsException extends BaseException
{
    public $status = 404;
    public $msg = '资源不存在';
    public $code = 10020;
}
