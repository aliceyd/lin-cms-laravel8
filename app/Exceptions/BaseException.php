<?php

namespace App\Exceptions;

use App\Lib\CodeMessage;

class BaseException extends \Exception
{
    //HTTP状态码
    public $status = 400;

    //错误具体信息
    public $msg = '参数错误';

    //自定义的错误码
    public $code = 10030;

    function __construct($code = null, $status = null)
    {
        if (is_numeric($code) && is_array($status)) {
            $re = (new CodeMessage())->codeMessage($code, $status);
            $this->msg = $re['msg'];
            $this->code = $re['code'];
        } else if (is_numeric($code)) {
            $re = (new CodeMessage())->codeMessage($code);
            $this->msg = $re['msg'];
            $this->code = $re['code'];
        }
        if (is_numeric($status)) {
            $this->status = $status;
        }
        if (is_array($code)) {
            if (array_key_exists('status', $code)) {
                $this->status = $code['status'];
            }
            if (array_key_exists('msg', $code)) {
                $this->msg = $code['msg'];
            }
            if (array_key_exists('code', $code)) {
                $this->code = $code['code'];
            }
        }
        parent::__construct($this->msg, $this->code);
    }
}
