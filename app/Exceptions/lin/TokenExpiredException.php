<?php

namespace App\Exceptions\lin;

class TokenExpiredException extends \Exception
{
    public $code = 401;
}
