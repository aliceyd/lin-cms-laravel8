<?php

namespace App\Exceptions\lin;

class TokenException extends LinException
{
    public $status = 401;
    public $msg = 'Token已过期或无效Token';
    public $code = 10040;
}
