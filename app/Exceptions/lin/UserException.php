<?php

namespace App\Exceptions\lin;

class UserException extends LinException
{
    public $status = 404;
    public $msg = '用户不存在';
    public $code = 10021;
}
