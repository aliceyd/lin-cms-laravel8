<?php

namespace App\Exceptions\lin;

use App\Exceptions\BaseException;

class LinException extends BaseException
{
    public $status = 400;
    public $msg = '失败';
    public $code = 10200;
}
