<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * 一个不会被报告的异常类型的列表
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * 认证异常时不被flashed的数据
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * 为应用程序注册异常处理回调
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    // 全局处理器
    public function render($request, $e)
    {
        /*
         * 404 | 未找到（请求资源不存在）
         * 401 | 未认证 (需要登录)
         * 403 | 没有权限
         * 400 | 错误的请求（URL或参数不正确）
         * 422 | 验证失败
         * 500 | 服务器错误
         */
        // 如果config配置debug为 true==>debug 模式的话让laravel自行处理
        /*if (config('app.debug')) {
            return parent::render($request,$e);
        }*/
        if ($e instanceof BaseException) { #api异常dd($e);
            $result = [
                "code" => $e->code,
                "message" => $e->msg,
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, $e->status);
        }
        if ($e instanceof \Illuminate\Validation\ValidationException) { #验证异常
            //$e->errors() 得到验证的所有错误信息(一个关联二维数组)
            $msg = [];
            foreach ($e->errors() as $key => $error) {
                $msg[$key] = implode($error);
            }
            $result = [
                "code" => 10030,
                "message" => $msg,
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, 422);
        }
        if ($e instanceof \Illuminate\Database\QueryException) { #SQL异常
            $result = [
                "code" => 99999,
                "message" => $e->getMessage(),
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, 500);
        }
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $result = [
                "code" => 99999,
                "message" => '地址不存在',
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, 400);
        }
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            $result = [
                "code" => 99999,
                "message" => '错误的请求:' . $e->getMessage(),
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, 400);
        }
        if ($e instanceof \ParseError || $e instanceof \ErrorException ) {
            $result = [
                "code" => 99999,
                "message" => '语法错误:' . $e->getMessage(),
                "request" => $request->method() . ' ' . $request->path()
            ];
            return response()->json($result, 500);
        }
        /*$result = [
            "code" => 99999,
            "message" => '服务器内部错误，不想告诉你',
            "request" => $request->method() . ' ' . $request->path()
        ];
        Logs::error($result['request_url'], $e, 'error'); #记录日志
        return response()->json($result, 500);*/
        return parent::render($request, $e);
    }
}
