<?php

namespace App\Services\token;

use App\Exceptions\lin\TokenExpiredException;
use App\Exceptions\lin\TokenException;
use App\Lib\token\Token as TokenUtils;
use App\Lib\token\TokenConfig;
use Illuminate\Support\Facades\Request;

class LoginToken
{
    private static $instance = null;

    private $tokenConfig;

    private function __construct()
    {
        $config = config('token');
        $this->tokenConfig = (new TokenConfig())
            ->dualToken($config['enable_dual_token'])
            ->setAlgorithms($config['algorithms'])
            ->setIat(time())
            ->setIss($config['issuer'])
//            ->setAud($config['audience'])
//            ->setJti($config['id'])
            ->setAccessSecretKey($config['access_secret_key'])
            ->setAccessExp($config['access_expire_time'])
            ->setRefreshSecretKey($config['refresh_secret_key'])
            ->setRefreshExp($config['refresh_expire_time']);
    }

    public static function getInstance(): LoginToken
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 获取令牌
     * @param array $extend 要插入到令牌扩展字段中的信息
     * @return array
     * @throws \Exception
     */
    public function getToken(array $extend): array
    {
        $this->tokenConfig->setExtend($extend);
        return TokenUtils::makeToken($this->tokenConfig);
    }

    /**
     * 刷新令牌
     * @param string $refreshToken
     * @return array
     * @throws TokenException
     */
    public function refresh(string $refreshToken): array
    {
        try {
            return TokenUtils::refresh($refreshToken, $this->tokenConfig);
        } catch (TokenException $e) {
            throw new TokenException(['msg' => '令牌签名错误', 'code' => 10042]);
        } catch (TokenExpiredException $e) {
            throw new TokenException(['msg' => '令牌已过期，请重新登录', 'code' => 10052]);
        }
    }

    /**
     * 校验令牌
     * @param string|null $token
     * @param string $tokenType
     * @return array|mixed
     * @throws TokenException
     */
    public function verify(string $token = null, string $tokenType = 'access'): array
    {
        $token = $token ?: $this->getTokenFromHeaders();
        try {
            return TokenUtils::verifyToken($token, $tokenType, $this->tokenConfig);
        } catch (TokenException $e) {
            throw new TokenException(['msg' => '令牌签名错误', 'code' => 10041]);
        } catch (TokenExpiredException $e) {
            throw new TokenException(10051);
        }
    }

    /**
     * 获取指定令牌扩展内容字段的值
     * @param string $val
     * @return mixed
     */
    public function getExtendVal(string $val)
    {
        return $this->getTokenExtend()->$val;
    }

    public function getCurrentUid()
    {
        return $this->getExtendVal('id');
    }

    public function getCurrentUserName()
    {
        return $this->getExtendVal('username');
    }

    /**
     * @return mixed
     */
    public function getTokenExtend()
    {
        return $this->tokenConfig->getDecodeToken()->getClaim('extend');
    }

    /**
     * @return string
     * @throws TokenException
     */
    public function getTokenFromHeaders(): string
    {
        $authorization = Request::header('authorization');

        if (!$authorization) {
            throw new TokenException(['msg' => '请求未携带Authorization信息', 'code' => 10012]);
        }

        list($type, $token) = explode(' ', $authorization);

        if ($type !== 'Bearer') throw new TokenException(['msg' => '接口认证方式需为Bearer', 'code' => 10000]);

        if (!$token || $token === 'undefined') {
            throw new TokenException(['msg' => '尝试获取的Authorization信息不存在', 'code' => 10013]);
        }

        return $token;
    }
}
