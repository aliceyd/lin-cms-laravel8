<?php

return [
    # 是否开启双令牌模式，本项目必须
    'enable_dual_token' => true,
    # 令牌算法类型
    'algorithms' => 'HS256',
    # 令牌签发者
    'issuer' => 'lin-cms-laravel8',
    # accessToken秘钥
    'access_secret_key' => 'w.kx(c82jkA',
    # accessToken过期时间，单位秒
    'access_expire_time' => 604800, //2h-7200 24h-86400
    # refreshToken秘钥
    'refresh_secret_key' => 'xUh.@3s8A8',
    # refreshToken过期时间，建议设置较长时间
    # 在有效期内可用于刷新accessToken，单位秒
    'refresh_expire_time' => 604800,

    # jwt参数
    'audience' => 'http://example.org',//配置访问群体 用户aud
    'id' => '4f1g23a12aa',//唯一身份标识，主要用来作为一次性token,从而回避重放攻击 JTI// jwt密钥
    'secret' => 'mBC5v1sOKVvbdEitdSBenu59nfNfhwkedkJVNabosTw', //jwt密钥
];
