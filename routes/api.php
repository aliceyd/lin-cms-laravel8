<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// 首次部署显示欢迎用的，部署完成后可以删掉掉该路由
//Route::get('/', function () {
//    return view('welcome');
//});
Route::middleware(['cors', 'auth'])->group(function () {
    Route::group(['namespace' => 'cms', 'prefix' => 'cms'], function () {
        // 账户相关接口分组
        Route::group(['prefix' => 'user'], function () {
            // 登陆接口
            Route::post('login', 'User@login')->withoutMiddleware('auth');
            //->middleware('throttle:1,2');//次数上限，分钟
            // 刷新令牌
            Route::get('refresh', 'User@refresh')->withoutMiddleware('auth');
            // 更新用户信息
            Route::put('', 'User@update');
            // 修改密码
            Route::put('change_password', 'User@changePassword');
            // 查询自己拥有的权限
            Route::get('permissions', 'User@getAllowedApis');
            // 注册一个用户
            Route::post('register', ['uses' => 'User@register', 'auth' => ['创建用户', '管理员', 'hidden']]);
            // 查询自己信息
            Route::get('information', 'User@getInformation');
        });
        // 管理类接口
        Route::group(['prefix' => 'admin'], function () {
            // 查询所有可分配的权限
            Route::get('permission', ['uses' => 'Admin@authority', 'auth' => ['查询所有可分配的权限', '管理员', 'hidden']])->withoutMiddleware('auth');
            // 查询所有用户
            Route::get('users', ['uses' => 'Admin@getUsers', 'auth' => ['查询所有用户', '管理员', 'hidden']]);
            // 修改用户密码
            Route::put('user/{id}/password', ['uses' => 'Admin@changeUserPassword', 'auth' => ['修改用户密码', '管理员', 'hidden']]);
            // 删除用户
            Route::delete('user/{id}', ['uses' => 'Admin@deleteUser', 'auth' => ['删除用户', '管理员', 'hidden']]);
            // 更新用户信息
            Route::put('user/{id}', ['uses' => 'Admin@updateUser', 'auth' => ['管理员更新用户信息', '管理员', 'hidden']]);
            // 查询所有权限组
            Route::get('group/all', ['uses' => 'Admin@getGroupAll', 'auth' => ['查询所有权限组', '管理员', 'hidden']]);
            // 查询一个权限组及其权限
            Route::get('group/{id}', ['uses' => 'Admin@getGroup', 'auth' => ['查询一个权限组及其权限', '管理员', 'hidden']]);
            // 新建权限组
            Route::post('group', ['uses' => 'Admin@createGroup', 'auth' => ['新建权限组', '管理员', 'hidden']]);
            // 更新一个权限组
            Route::put('group/{id}', ['uses' => 'Admin@updateGroup', 'auth' => ['更新一个权限组', '管理员', 'hidden']]);
            // 删除一个权限组
            Route::delete('group/{id}', ['uses' => 'Admin@deleteGroup', 'auth' => ['删除一个权限组', '管理员', 'hidden']]);
            // 分配单个权限
            Route::post('permission/dispatch', ['uses' => 'Admin@dispatchPermission', 'auth' => ['分配单个权限', '管理员', 'hidden']]);
            // 添加多个权限
            Route::post('permission/dispatch/batch', ['uses' => 'Admin@dispatchPermissions', 'auth' => ['分配多个权限', '管理员', 'hidden']]);
            // 删除多个权限
            Route::delete('permission/remove', ['uses' => 'Admin@removePermissions', 'auth' => ['删除多个权限', '管理员', 'hidden']]);
        });
        // 日志类接口
        Route::group(['prefix' => 'log'], function () {
            // 查询所有日志
            Route::get('', ['uses' => 'Log@getLogs', 'auth' => ['查询所有日志', '日志']]);
            // 搜索日志
            Route::get('search', ['uses' => 'Log@searchLogs', 'auth' => ['搜索日志', '日志']]);
            // 查询日志记录的用户
            Route::get('users', ['uses' => 'Log@getUsers', 'auth' => ['查询日志记录的用户', '日志']]);
        });
        // 上传图片文件类接口
        Route::post('file', 'file@uploadImage');
        // API接口
        Route::get('api', 'api@getApis');
    });
    Route::group(['namespace' => 'v1', 'prefix' => 'v1'], function () {
        Route::group(['prefix' => 'book'], function () {
            // 查询所有图书
            Route::get('', 'Book@getBooks');
            // 新建图书
            Route::post('', ['uses' => 'Book@create']);
            // 查询指定bid的图书
            Route::get('{id}', 'Book@getBook');
            // 更新图书
            Route::put('/{id}', 'Book@update');
            // 删除图书
            Route::delete('/{id}', ['uses' => 'Book@delete', 'auth' => ['删除图书', '图书']]);
        });
    });
});

