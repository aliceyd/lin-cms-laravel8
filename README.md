<h1 align="center">
  <a href="http://doc.cms.talelin.com/">
  <img src="https://consumerminiaclprd01.blob.core.chinacloudapi.cn/miniappbackground/sfgmember/lin/left-logo.png" width="250"/></a>
  <br>
  Lin-CMS-Laravel8
</h1>

<p align="center">
  <img src="https://img.shields.io/badge/PHP-%3E%3D7.3-blue.svg" alt="php version" data-canonical-src="https://img.shields.io/badge/PHP-%3E%3D7.3-blue.svg" style="max-width:100%;"></a>
  <a href="https://learnku.com/docs/laravel/8.x/" rel="nofollow"><img src="https://img.shields.io/badge/Laravel-8.x-red.svg" alt="Laravel version" data-canonical-src="https://img.shields.io/badge/Laravel-8.x-red.svg" style="max-width:100%;"></a>
  <img src="https://img.shields.io/badge/license-license--2.0-lightgrey.svg" alt="LISENCE" data-canonical-src="https://img.shields.io/badge/license-license--2.0-lightgrey.svg" style="max-width:100%;"></a>
</p>

# 简介

## 预防针

-   本项目非官方团队出品，仅出于学习、研究目的丰富下官方项目的语言支持，官方团队产品了解请访问 [TaleLin](https://github.com/TaleLin)
-   本项目采取后跟进官方团队功能的形式，即官方团队出什么功能，这边就跟进开发什么功能，开发者不必担心前端适配问题。
-   在上一点的基础上，我们会尝试加入一些自己的想法并实现。
-   局限于本人水平，有些地方还需重构，已经纳入了计划中，当然也会有我没考虑到的，希望有更多人参与进来一起完善，毕竟 PHP 作为世界上最好的语言不能缺席。

## 注意

**Lin-CMS 是工程类开源项目，不能保证无缝升级**

## 文档地址

[http://doc.cms.talelin.com/](http://doc.cms.talelin.com/)

## 线上 Demo

[http://face.cms.talelin.com/](http://face.cms.talelin.com/)

## 案例

[http://sleeve.talelin.com/](http://sleeve.talelin.com/)

## 线上文档地址(参照官方文档)

[https://doc.cms.talelin.com/api/](https://doc.cms.talelin.com/api/)

## 什么是 Lin CMS？

Lin-CMS 是林间有风团队经过大量项目实践所提炼出的一套内容管理系统框 架。Lin-CMS 可以有效的帮助开发者提高 CMS 的开发效率。

> 本项目是基于 Laravel 8.x 的 Lin CMS 后端实现。需要前端？请访问 [前端仓库](https://github.com/TaleLin/lin-cms-vue)。

## Lin CMS 的特点

Lin CMS 的构筑思想是有其自身特点的。下面我们阐述一些 Lin 的主要特点。

### Lin CMS 是一个前后端分离的 CMS 解决方案

Lin 既提供后台的支撑，也有一套对应的前端系统，你也可以选择不同的后端实现， 如 koa 和 flask

### 框架本身已内置了 CMS 常用的功能

Lin 已经内置了 CMS 中最为常见的需求：用户管理、权限管理、日志系统等。开发者只需要集中精力开发自己的 CMS 业务即可

### Lin CMS 本身也是一套开发规范

Lin CMS 除了内置常见的功能外，还提供了一套开发规范与工具类。 Lin CMS 只需要开发者关注自己的业务开发，它已经内置了很多机制帮助开发者快速开发自己的业务

## 所需基础

由于 Lin 采用的是前后端分离的架构，所以你至少需要熟悉 PHP 和 Vue。

Lin 的服务端框架是基于 [laravel8.x](https://learnku.com/docs/laravel/8.x/) 的，所以如果你比较熟悉 Laravel 的开发模式，那将可以更好的使用本项目。但如果你并不熟悉 Laravel，我们认为也没有太大的关系，因为框架本身已经提供了一套完整的开发机制，你只需要在框架下用 PHP 来编写自己的业务代码即可。照葫芦画瓢应该就是这种感觉。

但前端不同，前端还是需要开发者比较熟悉 Vue 的。但我想以 Vue 在国内的普及程度，绝大多数的开发者是没有问题的。这也正是我们选择 Vue 作为前端框架的原因。如果你喜欢 React Or Angular，那么加入我们，为 Lin 开发一个对应的版本吧。

# 快速开始

## Server 端必备环境

-   安装 MySQL（version： 5.7+）

-   安装 PHP 环境(version： 7.1+)

## 获取工程项目

```bash
git clone https://gitee.com/aliceyd/lin-cms-laravel.git
```

> 执行完毕后会生成 lin-cms-laravel8 目录

## 安装依赖包

执行命令前请确保你已经安装了 composer 工具

```bash
# 进入项目根目录
cd lin-cms-laravel8
# 先执行以下命令，全局替换composer源，解决墙的问题
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
# 接着执行以下命令安装依赖包
composer install
```

## 目录结构

```
根目录
├─app                   应用目录
│  ├─Console            模块目录
│  ├─Events             模块目录
│  ├─Exceptions         自定义异常类库目录
│  ├─Http               模块目录
│  │  ├─Controller      控制层目录
│  │  │  ├─cms          开发CMS API目录
│  │  │  └─v1           开发普通API目录
│  │  ├─Middleware      中间件目录
│  │  └─Requests        验证器目录
│  │     ├─...          API验证器文件
│  │     └─rule         局部验证规则
│  ├─Lib                类库文件目录
│  │  ├─auth            权限校验类库目录
│  │  ├─file            文件上传类库目录
│  │  ├─log             日志处理类库目录
│  │  └─token           令牌类库目录
│  │
│  ├─Models             模型层目录
│  ├─Provider           应用服务供应目录
│  ├─Services           复杂逻辑处理目录
│  └─helper.php         助手函数文件
│
├─bootstrap             框架启动文件
│
├─config                应用配置目录
│  ├─app.php            应用配置
│  ├─auth.php           权限配置
│  ├─broadcasting.php   广播配置
│  ├─cache.php          缓存配置
│  ├─cors.php           跨域配置
│  ├─database.php       数据库配置
│  ├─filesystems.php    文件配置
│  ├─hashing.php        哈希散列配置
│  ├─logging.php        日志记录配置
│  ├─mail.php           邮件配置
│  ├─queue.php          队列配置
│  ├─services.php       第三方服务配置
│  ├─session.php        Session配置
│  ├─token.php          Token配置
│  ├─view.php           view配置
│  └─...                更多
│
├─database              数据库迁移文件目录
│
├─public                WEB目录（对外访问目录）
│  ├─index.php          入口文件
│  └─.htaccess          用于apache的重写
│
├─resources             资源文件目录
│  ├─css                样式文件
│  ├─js                 JS文件
│  ├─lang               语言文件
│  └─view               视图文件
│
├─route                 路由定义目录
│  ├─api.php            api路由
│  ├─channels.php       注册广播路由
│  ├─console.php        闭包路由
│  ├─web.php            web路由
│  └─...                更多
│
├─storage               存储文件目录
│  ├─app                应用文件
│  ├─framework          框架文件
│  ├─logs               日志文件
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  └─logo.png           框架LOGO文件
│
├─tests                 测试目录
├─vendor                第三方类库目录（Composer依赖库）
├─composer.json         composer 定义文件
├─README.md             README 文件
├─artisan               命令行入口文件
└─.env.example          配置文件
```

## 数据库配置

Lin 需要你自己在 MySQL 中新建一个数据库，名字由你自己决定。例如，新建一个名为 lin-cms 的数据库。接着，我们需要在工程中进行一项简单的配置。使用编辑器打开 Lin 工程根目录下`/config/database.php`，找到如下配置项：

```php
'mysql' => [
    'driver' => 'mysql',
    'url' => env('DATABASE_URL'),
    'host' => env('DB_HOST', '127.0.0.1'),
    'port' => env('DB_PORT', '3306'),
    'database' => env('DB_DATABASE', 'forge'),
    'username' => env('DB_USERNAME', 'forge'),
    'password' => env('DB_PASSWORD', ''),
    'unix_socket' => env('DB_SOCKET',
    //省略后面一堆的配置项
]
```

在`.env`(复制一份`.env.example`重命名为`.env`)，找到如下配置项：

```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=lin-cms
DB_USERNAME=root
DB_PASSWORD=
```

**请务必根据自己的实际情况修改此配置项**

## 数据迁移

> 如果你已经部署过官方团队其他版本的 Lin-cms 后端，并且已经生成了相应基础数据库表，可以略过数据迁移章节，但必须将原来 lin_user 表中 super 记录删除(密码加密方式不一致，会导致登陆失败)，并在根目录下运行`php artisan db:seed --class=SuperSeeder` ,这条命令会为你在 lin_user 表中插入一条记录,即 super，如果你并没有部署过其他版本的 LinCMS 后端，请继续阅读后面的内容

配置完数据库连接信息后，我们需要为数据库导入一些核心的基础表，在项目根目录中，打开命令行，输入：

```bash
 php artisan migrate
```

当你看到如下提示时，说明迁移脚本已经启动并在数据库中生成了相应的基础数据库表

```php
Migration table created successfully.
Migrating: 2021_08_19_090128_create_book_table
Migrated:  2021_08_19_090128_create_book_table (33.03ms)
Migrating: 2021_08_19_095217_create_lin_file_table
Migrated:  2021_08_19_095217_create_lin_file_table (12.94ms)
Migrating: 2021_08_19_095721_create_lin_group_table
Migrated:  2021_08_19_095721_create_lin_group_table (8.53ms)
Migrating: 2021_08_19_095921_create_lin_group_permission_table
Migrated:  2021_08_19_095921_create_lin_group_permission_table (14.47ms)
Migrating: 2021_08_19_100149_create_lin_log_table
Migrated:  2021_08_19_100149_create_lin_log_table (14.85ms)
Migrating: 2021_08_19_100218_create_lin_permission_table
Migrated:  2021_08_19_100218_create_lin_permission_table (9.49ms)
Migrating: 2021_08_19_100238_create_lin_user_table
Migrated:  2021_08_19_100238_create_lin_user_table (16.00ms)
Migrating: 2021_08_19_100308_create_lin_user_group_table
Migrated:  2021_08_19_100308_create_lin_user_group_table (12.06ms)
Migrating: 2021_08_19_100335_create_lin_user_identity_table
Migrated:  2021_08_19_100335_create_lin_user_identity_table (10.54ms)
```

迁移成功后我们需要为数据表插入一些数据，方便你后续在前端项目中登陆和测试，继续在命令行中输入：

```bash
1 单个文件执行
php artisan db:seed --class=文件名

2 批量执行
php artisan db:seed
```

当你看到如下提示时，说明迁移脚本已经启动并在数据表中创建了记录

```php
Database seeding completed successfully.
```

## 运行

如果前面的过程一切顺利，项目所需的准备工作就已经全部完成，这时候你就可以试着让工程运行起来了。在工程的根目录打开命令行，输入：

```bash
php artisan serve --port 5000 //启动laravel内置的Web服务器
```

启动成功后会看到如下提示：

```php
Starting Laravel development server: http://127.0.0.1:5000
You can exit with `CTRL-C`
```

打开浏览器，访问`http://127.0.0.1:5000`，你会看到一个欢迎界面，至此，Lin-cms-laravel8 部署完毕，可搭配[lin-cms-vue`0.4.0`](https://github.com/TaleLin/lin-cms-vue)使用了（用户名:super，密码：123456）。

## 讨论交流

QQ 群搜索：Lin CMS 官方交流群 或 814597236

微信公众号搜索：林间有风
